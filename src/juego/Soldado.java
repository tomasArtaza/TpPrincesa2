package juego;

import java.awt.Color;
//import java.awt.*;
import entorno.Entorno;
//import java.util.Random;
//import entorno.Herramientas;

public class Soldado {
	private double x;
	private double y;
	private double diametro;
//	private double angulo;
//	private Image imagen;
	
	
	Soldado(double x, double y, double diametro /* imagen */) {
		this.x 		  = x;
		this.y 		  = y;
		this.diametro = diametro;
//		this.angulo   = 0;
//		this.imagen = Herramientas.cargarImagen(" nombre de la imagen.img ");
	}

	public static Soldado[] inicializar() {
		Soldado[] soldados = new Soldado[3];
		soldados[0] = new Soldado(400, 515, 45);
		soldados[1] = new Soldado(600, 515, 45);
		soldados[2] = new Soldado(800, 515, 45);
		return soldados;
	}
	
	public static void dibujarContorno(Entorno entorno, Soldado[] soldados) {		// BORRAR CUANDO NO SE NECESITE
		for (int i = 0; i < soldados.length; i++) {
			entorno.dibujarCirculo(soldados[i].x, soldados[i].y, soldados[i].diametro, Color.red);
		}
	}

//	public static void dibujar(Entorno entorno, Soldado[] soldados) {
//		for (int i = 0; i < soldados.length; i++) {
//			entorno.dibujarImagen(soldados[i].imagen, soldados[i].x, soldados[i].y, soldados[i].angulo);
//		}
//	}
	
	public static void mover(Soldado[] soldados) {
		for (int i = 0; i < soldados.length; i++) {
			soldados[i].mover();
		}
	}

	private void mover() {	// FALTA POSICION RANDOM DE SOLDADOS Y NO-SUPERPOSICION
		this.x -= 2.0;
		if (this.x <= -12.5) {
//			Random random = new Random();		// para que varie la distancia entre obstaculos
//			this.x = 1000 + random.nextInt(500);
			this.x = 812.5;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
