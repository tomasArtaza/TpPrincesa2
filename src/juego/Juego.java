package juego;

import java.awt.Color;
import entorno.Entorno;
import entorno.InterfaceJuego;
//import java.awt.Color;
// PREGUNTAR A LOS PROFES SI SE PUEDE CAMBIAR EL PJ Y EL NOMBRE DEL JUEGO
// PREGUNTAR SOBRE EL RETROCESO DEL PERSONAJE
// CAMBIAR NOMBRES A GETTERS PORQUE QUEDAN FEOS
// ACORTAR NOMBRES LARGOS DE VARIABLES (OBSTACULOS = OBS)
public class Juego extends InterfaceJuego {
	private Entorno 	entorno;
	private Pikachu 	pikachu;
	private Trueno 		trueno;			// podria ser una lista o arreglo para tener varios disparos
	private Obstaculo[] obstaculos;
	private Soldado[] 	soldados;
	private Fondo 		fondo;
	private int 		puntos;
	private int 		vidas;
//	-----------------------------------------------------------------------------------
	public Juego() {
		this.entorno = new Entorno(this, "Pika Pika", 800, 600);
		fondo 		= new Fondo();
		pikachu 	= new Pikachu();
		trueno		= new Trueno();
		obstaculos	= Obstaculo.inicializar();
		soldados	= Soldado.inicializar();
		puntos		= 0;
		vidas		= 3;
		this.entorno.iniciar();
	}
//	-----------------------------------------------------------------------------------
	public void tick() {
		// FALTA UN while(vidas > 0) Y UN GAME OVER
		fondo		.dibujar(entorno);
		pikachu		.dibujarContorno(entorno);				// borrar cuando no se use
		pikachu		.dibujar(entorno);
//		Soldado		.dibujar(entorno, soldados);	faltan las imagenes
//		Obstaculo	.dibujar(entorno, obstaculos);	faltan las imagenes
		Obstaculo	.dibujarContorno(entorno, obstaculos);	// borrar
		Soldado		.dibujarContorno(entorno, soldados);	// borrar
		Obstaculo	.mover(obstaculos);
		Soldado		.mover(soldados);
		mostrarPuntos();
		mostrarVidas();
		
		if (entorno.sePresiono(entorno.TECLA_ARRIBA) || pikachu.salto()) {
			pikachu.saltar(entorno);
		}
		if (entorno.estaPresionada(entorno.TECLA_DERECHA)) {
			pikachu.avanzar();
			
		}
		if (entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
			pikachu.retroceder();
			
		}
		if (pikachu.siChoca(obstaculos, soldados)) {
			vidas--;
		}
		if (entorno.sePresiono(entorno.TECLA_ESPACIO) || trueno.disparado()) {
			trueno.disparar();
		}
		if (trueno.siEliminaSoldado(soldados)) {
			puntos += 5;
		}
	}
//	-----------------------------------------------------------------------------------
	public void mostrarPuntos() {
		entorno.cambiarFont("Comic Sans MS", 40, Color.darkGray);
		entorno.escribirTexto("Puntos: " + puntos, 603, 103);	// INFO EN PANTALLA
		entorno.cambiarFont("Comic Sans MS", 40, Color.RED);	// demasiadas lineas
		entorno.escribirTexto("Puntos: " + puntos, 600, 100);	// se podrian meter en un metodo
	}
	public void mostrarVidas() {
		entorno.cambiarFont("Comic Sans MS", 40, Color.darkGray);
		entorno.escribirTexto("Vidas: " + vidas, 33, 103);
		entorno.cambiarFont("Comic Sans MS", 40, Color.GREEN);
		entorno.escribirTexto("Vidas: " + vidas, 30, 100);
	}
//	-----------------------------------------------------------------------------------


}
