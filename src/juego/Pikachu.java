package juego;

import java.awt.*;
import entorno.Entorno;
import entorno.Herramientas;
// PONER this. A TODAS LAS VARIABLES DE INSTANCIA
public class Pikachu {
	private double x;
	private double y;
	private double diametro;
	private double angulo;
	private Image imagen;
	private boolean salto;
	private double inercia;
	
	
	Pikachu() {
		this.x = 200;
		this.y = 510;
		this.diametro = 50;
		this.angulo = 2 * Math.PI;
		this.imagen = Herramientas.cargarImagen("imagenes/pika.gif");
		this.salto = false;		// cambia a true cuando se presiona arriba y a false cuando "toca" el suelo
		this.inercia = 0.0;		// la velocidad de ascenso y descenso
	}
	
	void dibujarContorno(Entorno entorno) {		// BORRAR CUANDO NO SE NECESITE
		entorno.dibujarCirculo(this.x, this.y, this.diametro, Color.yellow);
	}
	
	void dibujar(Entorno entorno) {
		entorno.dibujarImagen(this.imagen, this.x, this.y, this.angulo, 0.2);
	}
	
	boolean siChoca(Obstaculo[] obstaculos, Soldado[] soldados) {
		for (int i = 0; i < obstaculos.length; i++) {
			if (	this.x > obstaculos[i].getX() - obstaculos[i].getAncho()/2 &&
					this.x < obstaculos[i].getX() + obstaculos[i].getAncho()/2 &&
					this.y > obstaculos[i].getY() - obstaculos[i].getAlto()/2  &&
					this.y < obstaculos[i].getY() + obstaculos[i].getAlto()/2) {
				return true;
			}
		}
		return false;
	}


	void saltar(Entorno entorno) {
		if (entorno.sePresiono(entorno.TECLA_ARRIBA)) {
			this.inercia = -8;	// la velocidad con que salta
			this.salto = true;
		}
		if (this.salto) {
			this.y += this.inercia;
			this.inercia += 0.17;			// va restando velocidad al salto
		}
		if (this.y >= 510) {
			this.salto = false;				// cuando llega a la altura del suelo, deja de caer (salto = false)
			this.inercia = -8;				// reestablece velocidad de salto
		}
	}
	
	public void avanzar() {
		if (this.x < 350 && !this.salto) {
			this.x += 2;
		}
	}

	public void retroceder() {
		if (this.x > 40 && !this.salto) {
			this.x -= 2;
		}
	}
	

	public boolean salto() {
		return salto;
	}
	
	
}
